package fr.tibal.sonicflowerpot;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class FlowerPotPlayer {

    public static HashMap<Player, FlowerPotPlayer> players = new HashMap<>();
    private final Player player;
    private int compteur;
    private Integer firstIndex;
    private Boolean sens;
    private Integer lastIndex;
    private Integer nextIndex;

    public FlowerPotPlayer(Player player, Integer locationIndex) {
        this.player = player;
        this.compteur = 0;
        this.firstIndex = locationIndex;
        this.sens = null;
        players.put(player, this);
    }

    public Player getPlayer() {
        return player;
    }

    public void setCompteur(int compteur) {
        this.compteur = compteur;
    }

    public int getCompteur() {
        return compteur;
    }

    public static FlowerPotPlayer get(Player p){
        return players.get(p);
    }


    public Integer getFirstIndex() {
        return this.firstIndex;
    }

    public void setSens(Boolean sens) {
        this.sens = sens;
    }

    public Boolean getSens() {
        return this.sens;
    }

    public Integer getLastIndex() {
        return lastIndex;
    }

    public Integer getNextIndex() {
        return nextIndex;
    }

    public void setLastIndex(Integer currentI) {
        this.lastIndex = currentI;
        this.nextIndex = currentI + (this.sens?1:-1);
        if (nextIndex == 4 || nextIndex == -1){
            this.nextIndex = (this.sens?0:3);
        }
    }
}

package fr.tibal.sonicflowerpot;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ArrayList;

public class FlowerPot implements Listener {
    private final World world;
    private final Block top;
    private final Block bottom;
    private final Block left;
    private final Block right;
    private final Block[] ordre;
    private final Block center;
    private ArrayList<Player> inGame = new ArrayList<Player>();

    public FlowerPot(ConfigurationSection flowerPots, SonicFlowerPot sonicflowerpot) {
        this.world = Bukkit.getWorld(flowerPots.getString("world"));
        this.center = this.world.getBlockAt(flowerPots.getInt("x"), flowerPots.getInt("y"), flowerPots.getInt("z"));
        this.top = this.world.getBlockAt(flowerPots.getInt("x") + 1, flowerPots.getInt("y"), flowerPots.getInt("z"));
        this.bottom = this.world.getBlockAt(flowerPots.getInt("x") - 1, flowerPots.getInt("y"), flowerPots.getInt("z"));
        this.left = this.world.getBlockAt( flowerPots.getInt("x"), flowerPots.getInt("y"), flowerPots.getInt("z") + 1);
        this.right = this.world.getBlockAt( flowerPots.getInt("x"), flowerPots.getInt("y"), flowerPots.getInt("z") - 1);
        sonicflowerpot.getLogger().warning(top + " " + bottom + " " +  left + " " +  right);
        this.ordre = new Block[]{top, left, bottom, right};
        sonicflowerpot.pluginManager.registerEvents(this, sonicflowerpot);
    }

    public boolean isInGame(Player p) {
        return inGame.contains(p);
    }

    @EventHandler
    public void onPlayerStart(PlayerMoveEvent e) {
        Location currentLoc = e.getPlayer().getLocation();
        if (currentLoc.getWorld() != this.world){
            return;
        }
        Integer currentI = getLocationIndex(currentLoc);
        if (currentI != null) {
            if (!isInGame(e.getPlayer())) {
                inGame.add(e.getPlayer());
                new FlowerPotPlayer(e.getPlayer(), getLocationIndex(currentLoc));
            } else {
                FlowerPotPlayer fp = FlowerPotPlayer.get(e.getPlayer());
                int firstI = fp.getFirstIndex();
                if (fp.getSens() == null) {
                    if (currentI != firstI) {
                        if (firstI == 0 || firstI == 3) {
                            fp.setSens(currentI == 0);
                        } else {
                            fp.setSens(firstI < currentI);
                        }
                        fp.setLastIndex(currentI);
                    }
                } else {
                    int next = fp.getNextIndex();
                    if (next == currentI) {
                        if (currentI.equals(fp.getFirstIndex())) {
                            fp.setCompteur(fp.getCompteur() + 1);
                            e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 1, 1);
                            if (fp.getCompteur() >= 10) {
                                e.getPlayer().sendMessage(ChatColor.YELLOW + "Félicitation tu as fait 10 fois le tour du pot de fleur tu dois bien te faire chier");
                                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                                fp.setCompteur(0);
//                            TODO Donner l'achievement
                            }
                        }
                        if (next == 0 || next == 3) {
                            fp.setLastIndex(next);
                        } else {
                            fp.setLastIndex(currentI);
                        }
                    }
                }

            }
        }else{
            if (currentLoc.distance(center.getLocation()) > 2.5 && isInGame(e.getPlayer())){
                resetPlayer(e.getPlayer());
                return;
            }
        }
    }

    public void resetPlayer(Player p ){
        inGame.remove(p);
        FlowerPotPlayer.players.remove(p);
    }

    public Integer getLocationIndex(Location location) {
        for (int i = 0; i < this.ordre.length; i++) {
            if (this.ordre[i].equals(location.getBlock())) {
                return i;
            }
        }
        return null;
    }

}

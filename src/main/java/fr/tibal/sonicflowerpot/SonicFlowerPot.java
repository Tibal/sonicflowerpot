package fr.tibal.sonicflowerpot;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class SonicFlowerPot extends JavaPlugin {

    public PluginManager pluginManager;
    private FlowerPot flowerPot;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        this.pluginManager = Bukkit.getPluginManager();
        this.flowerPot = new FlowerPot(this.getConfig().getConfigurationSection("FlowerPots"), this);
        this.getConfig();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
